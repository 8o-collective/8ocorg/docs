#y8k 

the finest minds of y8k discuss the possibility that they are embedded in fiction. ideally, their ideas are lampshaded as much as possible.

e.g.:

[[antwarnow]]> and here we are discussing the possibility of being in fiction. i'm just saying, if this _were_ fiction, it would be such an incredibly obnoxious nod / wink to the audience.

the discussion of whether or not they are fiction quickly evolves into whether or not the fiction they are in is _good_.